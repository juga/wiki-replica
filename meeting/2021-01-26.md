[[_TOC_]]

TL;DR: the 2021 roadmap was adopted, see the details here:

<https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021>

Followup of the [last meeting][] to complete the work on the 2021 roadmp.

 [last meeting]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/meeting/2021-01-19

# Roll call: who's there and emergencies

anarcat, gaba, hiro

[Problem with gmail][], not a rush but priority.

[problem with gmail]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40149

# Roadmap review

We looked at the [draft 2021 roadmap proposal][] anarcat sent last
week.

[draft 2021 roadmap proposal]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/roadmap/2021

## Need to have / nice to have / non-goals

 * need to prioritise fixing the blog (formatting, moderation), but
   those fixes will probably not come before Q3, because of capacity
 * we decided to *not* retire schleuder: hiro fixed a bunch of stuff
   yesterday, it should work better now. no need to retire it as we
   will still want encrypted mailing lists in the future
 * service admins; let's not reopen that discussion
 * added the bullseye upgrade to "nice to have", but not a hard
   priority for 2021 (and will be, along with the python3 upgrade, for
   2022)
 * search.tpo (#33106) and "web metrics" (#32996) are postponed to
   2022
 * people suggested retiring "testnet" in the survey, but we don't
   quite know what that is, so we presumably need to talk with the
   network team about this
 * we agreed to cover for some metrics: we updated [ticket 40125][]
   with the remaining services to reallocate. covering for a service
   means that TPA will reboot services and allocate disk/ram as
   needed, but we are not in a position to make major reengineering
   changes

 [ticket 40125]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40125

## Quarterly prioritization

 * there's a lot in Q1, but a lot of it is actually already done
 * sponsor 9 requires work from hiro, so we might have capacity
   problems

We added a few of the "needs to have" in the quarterly allocation to
make sure those are covered. We agreed we'd review the global roadmap
every quarter, and continue doing the monthly "kanban board" review
for the more daily progress.

# Next meeting

Going back to our regular programming, i have set a recurring meeting
on tuesdays, 1500UTC on the first tuesday of the month, for TPA.

# Metrics of the month

Skipped because last meeting was a week ago. ;)
