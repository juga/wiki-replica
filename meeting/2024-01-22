# Roll call: who's there and emergencies

no emergency. anarcat and lavamind online.

# Dashboard cleanup

Normal per-user check-in:

* https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&ssignee_username=anarcat
* https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=kez
* https://gitlab.torproject.org/groups/tpo/-/boards?scope=all&utf8=%E2%9C%93&assignee_username=lavamind

General dashboards:

* https://gitlab.torproject.org/tpo/tpa/team/-/boards/117
* https://gitlab.torproject.org/groups/tpo/web/-/boards
* https://gitlab.torproject.org/groups/tpo/tpa/-/boards

Had a long chat about metrics requirements, comments in https://gitlab.torproject.org/tpo/tpa/team/-/issues/41449

# 2024 roadmap

We reviewed the proposed roadmap. All seem well, although there was some surprise in the team at the reversal of the decision taken in Costa Rica regarding migrating from SVN to Nextcloud.

# Metrics of the month

 * hosts in Puppet: 88, LDAP: 88, Prometheus exporters: 169
 * number of Apache servers monitored: 35, hits per second: 759
 * number of self-hosted nameservers: 6, mail servers: 10
 * pending upgrades: 0, reboots: 0
 * average load: 0.58, memory available: 3.34 TiB/4.81 TiB, running processes: 391
 * disk free/total: 64.37 TiB/131.80 TiB
 * bytes sent: 380.84 MB/s, received: 252.72 MB/s
 * planned bookworm upgrades completion date: 2024-08-22
 * [GitLab tickets][]: 206 tickets including...
   * open: 0
   * icebox: 163
   * backlog: 23
   * next: 8
   * doing: 4
   * needs information: 4
   * needs review: 4
   * (closed: 3434)
    
 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Upgrade prediction graph lives at https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bookworm/

Now also available as the main Grafana dashboard. Head to <https://grafana.torproject.org/>, change the time period to 30 days, and wait a while for results to render.
